import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.mail.Session;
import javax.mail.Transport;

import static java.lang.System.out;

//https://stackoverflow.com/a/25499215

/**
 * Klasse zur Überprüfung der Eingabedaten. Ist zudem die bevorzugte Vorgehensweise, den Connector zu erstellen
 */
public class Authenticator {
    /**
     *
     * @param username Der Login-Nutzername
     * @param password Das Passwort, welches zum Nutzernamen dazugehört
     * @param session Die für das Verbinden mit dem Mailserver verwendete Session
     * @return Ob man sich mit den angegebenen Login-Daten über GMail in ein Postfach einloggen kann
     */
    public static boolean AreCredentialsValid(@NotNull String username, @NotNull String password, @NotNull Session session) {
        Transport transport;
        //Versuche eine Verbindung über SMTP mit dem SMTP Server von Gmail mit den angegebenen Login-Daten zu erstellen.
        // Wenn dies fehl schlägt sind die Login-Daten falsch
        try {
            transport = session.getTransport("smtp");
            transport.connect("smtp.gmail.com", username, password);
            transport.close();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    /**
     * Erstellt eine Verbindung zum GMail-Postfach anhand von eingegebenen Daten. Es wird solange Iteriert, bis korrekte Daten eingegeben wurden.
     * @return Einen Connector, mit welchem man sich mit einem GMail-Postfach verbinden kann.
     */
    @NotNull
    public static Connector CreateConnectorFromUserInput(){
        while(true){
            out.println("Geben Sie den Benutzernamen ein... ");
            String username = Input.GetString(false);
            out.println("Geben Sie das Passwort ein ... ");
            String password = Input.GetString(false);

            try{
                Connector.GetInstance().Init(username,password);
                return Connector.GetInstance();
            }
            catch (InvalidCredentialsException icex){
                out.println("\n\n\n\n");
                out.println("[Fehler] Eigegebener Benutzer und Passwort stimmen nicht");
            }

        }
    }

    /**
     * Erstelle einen Connector anhand von in die Methode eingegebenen Nutzerdaten. Kann bie falschen Nutzerdaten null zurückgeben.
     * Diese Methode sollte nur zum Testen verwendet werden.
     * @param user Der Nutzername
     * @param password Das Passwort
     * @return Einen Connector für die angegebenen Login-Daten, bzw null im falle von falschen Login-Daten
     */
    @Nullable
    public static Connector  Create(@NotNull String user,@NotNull String password){
        try{
            Connector.GetInstance().Init(user,password);
            return Connector.GetInstance();
        }catch (Exception e){
            return null;
        }
    }
}

import javax.mail.Folder;

import java.util.StringJoiner;

import static java.lang.System.*;

/**
 * Eine Hilfsklasse zum Anzeigen von Javamail Ordnern
 */
public class JavaMailFolderUtils {
    /**
     * Gebe die Eingabemaske zur Abfrage von Nachrichten in Ordnern an
     * @param root Der Wurzelordner.
     */
    public static void DisplayFolderUserInput(Folder root){

        //Gebe alle Ordner aus
        try{
            Folder[] folders = root.list("*");
            String FoldersString;
            //https://stackoverflow.com/questions/4790844/how-to-get-the-list-of-available-folders-in-a-mail-account-using-javamail
            // sowie https://javaee.github.io/javamail/docs/api/javax/mail/Folder.html#HOLDS_MESSAGES
            {
                StringJoiner sj = new StringJoiner("\n");
                sj.add("[0] Zurück zum Hauptmenü");
                for (int i = 0; i < folders.length; i++) {
                    Folder folder = folders[i];
                    //Man stelle sich den int von .getType als Bitmaske vor. Wenn das Bit für HOLDS_MESSAGES gesetzt ist kann es sein, dass Nachrichten drinnen vorkommen können
                    if ((folder.getType() & Folder.HOLDS_MESSAGES) != 0) {
                        int messageCount = folder.getMessageCount();
                        String name = folder.getFullName();
                        sj.add(String.format("[%d] %s (%d Nachricht(en))", i + 1, name, messageCount));

                    } else {
                        sj.add(String.format("[%d] Kann nicht aufgemacht werden", i + 1));
                    }
                }
                sj.add("----");
                sj.add("Geben Sie den zu öffnenden Ordner an...");
                FoldersString = sj.toString();
            }

            while (true)
            {
                out.println(FoldersString);
                int input = Input.GetIntegerFromRange(0,folders.length,false);
                if(input == 0)
                    return;
                if((folders[input-1].getType() & Folder.HOLDS_MESSAGES)  != 0){
                    try{
                        var folder = folders[input-1];
                        if(!folder.isOpen()){
                            folder.open(Folder.READ_ONLY);
                        }
                        var messages = folder.getMessages();
                        for(var message : messages){
                            try{
                                JavaMailMessageUtils.PrintMail(message);
                            }
                            catch (Exception e){
                                out.println("[FEHLER] Konnte Nachricht nicht lesen");
                            }
                        }
                        if(folder.isOpen())
                            folder.close(false);
                    }
                    catch (Exception e){
                        out.println("[FEHLER] Konnte Ordner nicht öffnen");
                    }
                }else{
                    out.println("Dieser Ordner kann nicht geöffnet werden. Anderen Ordner auswählen");
                }

            }


        }
        catch (Exception e){
            out.println("[FEHLER] Konnte Ordner nicht aufrufen.");
        }

    }

}


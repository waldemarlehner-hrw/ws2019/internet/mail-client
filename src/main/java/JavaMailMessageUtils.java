import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.mail.*;
import javax.mail.internet.MimeMultipart;

import java.io.IOException;

import static java.lang.System.out;

/**
 * Eine Hilfsklasse für JavaMail-Nachrichten
 */
public class JavaMailMessageUtils {
    /**
     * Schreibe den Inhalt einer Nachricht in die Konsole
     * @param message Die Nachricht, welche geschrieben werden soll
     */
    public static void PrintMail(@NotNull Message message){
        //Hier wird abgespeichert, wie der Ordner, in welchem die Nachricht steht vorgefunden wurde.
        boolean WasFolderOpen;
        try{
            WasFolderOpen = message.getFolder().isOpen();
            out.println("---------------------------------------");
            out.println("Betreff:");
            out.println(message.getSubject()+"\n");

            out.println("Von:");
            for(Address a :message.getFrom()){
                out.println(" > "+a.toString());
            }

            out.println("\nEmpfänger:");
            {
                //Wenn leer wird anstatt einem leeren Array null zurückgegeben. Wer kam auf diese Idee?!
                @Nullable
                Address[] to = message.getRecipients(Message.RecipientType.TO);
                @Nullable
                Address[] cc = message.getRecipients(Message.RecipientType.CC);
                @Nullable
                Address[] bcc = message.getRecipients(Message.RecipientType.BCC);
                if(to != null)
                for(Address a : to)
                    out.println("AN : "+a.toString());
                if(cc != null)
                for(Address a : cc)
                    out.println("CC : "+a.toString());
                if(bcc != null)
                for(Address a : bcc)
                    out.println("BCC: "+a.toString());
            }
            out.println("\nNachricht:\n");
            PrintMessageBody(message);
            out.println("---------------------------------------");

            if(WasFolderOpen && !message.getFolder().isOpen())
                message.getFolder().open(Folder.READ_ONLY);
            if(!WasFolderOpen && message.getFolder().isOpen())
                message.getFolder().close(false);
        }
        catch (Exception e){
            out.println("[FEHLER] Fehler beim Abrufen der Nachricht");
        }
    }

    /**
     * Gebe den Körper der Nachricht, also den tatsächlichen Inhalt der Nachricht in die Konsole aus. Die Nachricht kann unter anderem aus einer Multipart-Komponente
     * bestehen, welche wie ein Baum weitere Multipart oder Text-Elemente beinhalten kann. Diese werden in GetMultipartTextContent rekursiv ausgegeben
     * @param message Das Javamail Message-Objekt, welche die auszugebene Mail-Nachricht repräsentiert
     */
    //https://stackoverflow.com/a/34689614
    private static void PrintMessageBody(Message message){
        //Öffne Ordner wenn zu, um MIME-Typ zu bekommen
        if(!message.getFolder().isOpen())
            try{
                message.getFolder().open(Folder.READ_ONLY);
            }catch (MessagingException e){
                out.println("[FEHLER] Konnte Inhalt der Nachricht nicht lesen");
                return;
            }
        try{
            //Einfach Textnachricht. Kann sofort ausgegeben werden
            if(message.isMimeType("text/html") || message.isMimeType("text/plain"))
                out.println(message.getContent().toString());
            //Wenn weder Plaintext/HTML oder Multipart, gebe Fehler aus, springt zum Nächsten Catch Block
            if(!message.isMimeType("multipart/*")){
                throw new MessagingException();
            }
            //Es bleiben nur multipart Nachrichten
            MimeMultipart mimeMultipart = (MimeMultipart) message.getContent();
            out.println(GetMultipartTextContent(mimeMultipart));

        }
        catch (Exception e){
            out.println("[FEHLER] Konnte Inhalt nicht lesen");
        }
        try{
            message.getFolder().close(false);
        }
        catch (Exception e){
            out.println("[FEHLER] Konnte Ordner nicht schließen");
        }


    }

    /**
     * Eine Multipart-Nachricht besteht aus mehreren "Subkomponenten".
     * Gebe alle Komponenten mit dem MIME text/html oder text/plain aus.
     * D.h. Komponenten wie Anhänge werden Ignoriert
     * @param mimeMultipart Eine Multipart Komponente, welche beliebig viele Kinder beinhaltet. Wenn mimeType des Kindes text/html oder text/plain wird der String dazu zur Nachricht hinzugefügt
     * @return Gibt die Nachrichten (text/html und text/plain) als String zurück
     * @throws MessagingException
     * @throws IOException
     */
    //https://stackoverflow.com/a/34689614
    @NotNull
    public static String GetMultipartTextContent(MimeMultipart mimeMultipart) throws MessagingException, IOException {
        StringBuilder sb = new StringBuilder();
        for(int i = 0;i<mimeMultipart.getCount();i++){
            BodyPart bodyPart = mimeMultipart.getBodyPart(i);
            if(bodyPart.isMimeType("text/plain") || bodyPart.isMimeType("text/html"))
                sb.append(bodyPart.getContent().toString());
            else if(bodyPart.isMimeType("multipart/*")){
                //Es handelt sich um eine verschachtelte Multipart-Komponente. Gehe diese auch durch und gebe Text-Komponenten aus
                sb.append(GetMultipartTextContent((MimeMultipart) bodyPart.getContent()));
            }
        }
        return sb.toString();
    }


}

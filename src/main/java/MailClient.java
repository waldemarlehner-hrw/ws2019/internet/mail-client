import javax.mail.Message;

import static java.lang.System.exit;
import static java.lang.System.out;

/**
 * Die Startklasse. Hier startet das Programm
 */
public class MailClient {

    static Connector connector;

    /**
     * Die main-Funktion. Wird beim Start der Applikation ausgeführtgibreupotpyrrckp
     * @param args Startparameter. Werden nicht verwendet
     */
    public static void main(String[] args) {
        printInitHeader();
        connector = Authenticator.CreateConnectorFromUserInput();
        //connector = Authenticator.Create("VMbq3X5eRsSbnDb3UWf4S6cF","gibreupotpyrrckp");
        if(connector == null){
            System.out.println("Fehler = Connect ist null");
            System.exit(-1);
        }
        MainMenu();
    }

    /**
     * Gebe einen "Header" in der Konsole aus
     */
    private static void printInitHeader() {
        System.out.println("------------------------------------");
        System.out.println("////// //Mail-Client von ///////////");
        System.out.println("/////// Waldemar und Philipp ///////");
        System.out.println("// Internet Wintersemester  19/20 //");
        System.out.println("------------------------------------");
    }

    /**
     * Zeigt das Hauptmenü an und ruft Funktionen basierend auf der Eingabe des Nutzers auf
     */
    public static void MainMenu() {

        out.println("Aktion auswählen : \n[1] für Ordneransicht  \n[2] Nachricht schreiben  \n[3] Suche  \n[4] Anwendung verlassen");

        int Selection= Input.GetIntegerFromRange(1,4,false);

        switch (Selection) {
            case 1:   /*Ordner*/
                Connector.GetInstance().FolderTraverse();

                break;
            case 2:  /* Nachricht schreiben*/
                MailMessage msg = MailMessageFactory.GenerateFromUserInput();
                if(msg != null){
                    Connector.GetInstance().SendMain(msg);
                }
                else{
                    out.println("[FEHLER] Es gab einen Fehler beim Erstellen der Nachricht. Kehre zum Hauptmenü zurück");
                    MainMenu();
                }

                break;
            case 3:  /* Suche*/
                out.println("Geben Sie einen Suchbegriff ein");
                Message[] msgs = Connector.GetInstance().SearchMailMessages(Input.GetString(false));
                out.println(msgs.length + " Nachrichten gefunden.");
                for(Message m : msgs){
                    JavaMailMessageUtils.PrintMail(m);
                }


                break;
            case 4:  /* Anwendung verlassen*/
                exit(0);
                break;
            default:
                out.println("Falsche Eingabe");


                break;
        }
        MainMenu();

    }


}

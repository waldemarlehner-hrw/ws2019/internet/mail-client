
import org.intellij.lang.annotations.RegExp;
import org.jetbrains.annotations.*;

import java.util.*;

//Wird importiert damit man anstatt System.out.println nur out.println schreiben kann
import static java.lang.System.*;


/**
 * Klasse, welche Nutzereingaben übernimmt und Restriktionen in der Eingabe inhält und auf Fehler überprüft
 */
public class Input {
      private static Scanner scanner = new Scanner(in);

      /**
       * Bekommt die Eingabe aus der Konsole als String. hierbei werden Whitespace-Characters am Anfang und Ende des Strings abgeschnitten
       * @return Die getrimmte Eingabe
       */
      @NotNull
      private static String getInput(){
            return scanner.nextLine().trim();
      }

      /**
       * Frage nach einer Eingabe eines einfachen Strings
       * @param allowEmpty Gibts an ob die Eingabe leer sein darf
       * @return Gibt den eingegebenen String bzw null im Falle einer leeren Eingabe zurück
       */
      @Nullable
      public static String GetString(boolean allowEmpty){
            out.println("Geben Sie einen String ein.\n");
            if(allowEmpty)
                  out.println(" Eine leere Eingabe ist erlaubt.\n");
            String value = getInput();
            if(value.length() == 0){
                  if(allowEmpty){
                        return null;
                  }else{
                        out.println("[FEHLER] Die Eingabe darf nicht leer sein. Bitte erneut versuchen\n");
                        return GetString(false);
                  }
            }
            return value;
      }

      /**
       * Gebe einen Integer zwischen einer unteren und oberen Schranke zurück
       * @param min Die untere Schranke
       * @param max Die obere Schranke
       * @param allowEmpty Darf die Eingabe leer sein
       * @return Gibt null für eine leere Eingabe bzw. einen Integer zwischen der unteren und oberen Schranke zurück
       * @throws IllegalArgumentException wenn Untere und Obere Schranke derselbe Wert sind
       */
      @Nullable
      public static Integer GetIntegerFromRange(int min,int max,boolean allowEmpty) throws IllegalArgumentException {
            if(min > max){
                  int temp = min;
                  max = min;
                  min = temp;
            }else if(min == max){
                  throw new IllegalArgumentException("Bounds cannot be the same value");
            }
            out.println(String.format("Geben Sie eine ganze Zahl zwischen %d und %d an.\n",min,max));
            if(allowEmpty)
                  out.println("Die Eingabe darf zudem leer sein.\n");
            String input = getInput();
            if(input.length() == 0){
                  if(allowEmpty)
                        return null;
                  else{
                        out.println("[FEHLER] Eine leere Eingabe ist nicht erlaubt.\n");
                        return GetIntegerFromRange(min,max,allowEmpty);
                  }
            }
            int i;
            try{
                  i = Integer.parseInt(input);
            }catch (NumberFormatException nfe){
                  out.println("[FEHLER] Die Eingabe ist keine ganze Zahl.\n");
                  return GetIntegerFromRange(min,max,allowEmpty);
            }
            if(i < min || i > max){
                  out.println(String.format("[FEHLER] Der eingegebene Wert %d liegt außerhalb der Grenzen [%d;%d]\n",i,min,max));
                  return GetIntegerFromRange(min,max,allowEmpty);
            }
            return i;
      }

      /**
       * Bekomme eine Ja/Nein Antwort
       * @return wahr oder falsch, je nachdem ob Y oder N eingegeben wurden
       */
      public static boolean GetYesNo(){
            out.println("Geben Sie Y oder N ein\n");
            String input = getInput();
            if ("y".equals(input.toLowerCase()) || "yes".equals(input.toLowerCase()) || "j".equals(input.toLowerCase()) || "ja".equals(input.toLowerCase())) {
                  return true;
            } else if ("n".equals(input.toLowerCase()) || "no".equals(input.toLowerCase()) || "nein".equals(input.toLowerCase())) {
                  return false;
            }
            out.println("[FEHLER] Nur Y oder N sind valide Eingaben.\n");
            return GetYesNo();
      }
}

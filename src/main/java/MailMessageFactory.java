import org.intellij.lang.annotations.RegExp;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.lang.System.out;

/**
 * Wird zum erstellen einer MailMessage verwendet. Beinhaltet zudem Hilfsfunktionen zum Arbeiten mit MailMessage
 */
public class MailMessageFactory {

    /**
     * Privater Konstruktor, damit keine Instanz erstellt werden kann
     */
    private MailMessageFactory() {}

    /**
     * Erstelle eine Nachricht durch Nutzereingaben in die Konsole
     * @return Eine Nachricht zum Absenden, oder null wenn der Vorgang abgebrochen wurde
     */
    @Nullable
    static MailMessage GenerateFromUserInput(){
        String subject;
        List<String> recipients = new ArrayList<>();
        List<String> messageLines = new ArrayList<>();
        out.println("Geben Sie die benötigten Eingaben ein. Sie können Fehler später korrigieren.");
        out.println("Geben Sie den Betreff ein");
        subject = Input.GetString(false);
        out.println("Geben Sie die Empfänger ein. \\ beendet die Eingabe der Empfänger");



        while(true){
            int i = 0;
            for(String s: recipients){
                i++;
                out.println("["+i+"] > "+s);
            }



            String input = Input.GetString(false);

            if(input.equals("\\")){
                if(recipients.size() == 0){
                    out.println("[FEHLER] Geben Sie mindestens einen Empfänger an.");
                }else{
                    break;
                }
            }else if(isMailString(input)){
                recipients.add(input);
                out.println("\n\n\n");
            }else{
                out.println("[FEHLER] Die Eingabe ist keine Email-Adresse");
            }
        }
        out.println("Geben Sie die Nachricht ein. Ein Enter erstellt eine neue Zeile. Um die Nachricht zu beenden muss in einer neuen Zeile das Zeichen '\\' eingegeben werden.");
        while(true){
            String input = Input.GetString(true);
            if(input == null)
                input = "";
            if(input.equals("\\")){
                if(messageLines.size() == 0){
                    out.println("[FEHLER] Die Nachricht darf nicht leer sein. Bitte mindestens eine Zeile angeben");
                }else{
                    break;
                }
            }
            messageLines.add(input);
        }

        //Eingabe der Nachricht ist fertig
        out.println("\n\n\n\n\n\n\n\n\n\n");
        out.println("----------------------");
        out.println("Betreff");
        out.println(subject);
        out.println("An");
        {
            StringBuilder sb = new StringBuilder();

            for(String recipient : recipients)
                sb.append("\n > ").append(recipient);
            out.println(sb.toString());
        }
        out.println("Nachricht");
        {
            StringBuilder sb = new StringBuilder();

            for(String msgLine : messageLines)
                sb.append("\n > ").append(msgLine);
            out.println(sb.toString());
        }

        return ChangeUserInput(recipients,messageLines,subject);


    }

    /**
     * Nimmt die Komponenten einer MailMessage, und erlaubt die Abänderung der Werte
     * @param to Liste an Empfängern
     * @param messageLines Zeilen der Nachricht als Liste
     * @param topic Betreff
     * @return Eine fertige Nachricht
     */
    @Nullable
    private static MailMessage ChangeUserInput(@NotNull List<String> to,@NotNull List<String> messageLines,@NotNull String topic){
        while (true)
        {
            out.println("----------------------");
            out.println("[1] > Betreff bearbeiten");
            out.println("[2] > Empfänger bearbeiten");
            out.println("[3] > Nachricht bearbeiten");
            out.println("[4] > Absenden");
            out.println("[5] > Abbrechen");
            out.println("----------------------");
            Integer input = Input.GetIntegerFromRange(1,5,false);
            if(input == null)
                input = -1;
            switch (input){
                case 1:
                    topic = Input.GetString(false);
                    break;
                case 2:
                    EditRecipientList(to);
                    break;
                case 3:
                    EditStringList(messageLines);
                    break;
                case 4:
                    out.println("Absenden der Nachricht bitte bestätigen...");
                    if(Input.GetYesNo()){
                        String[] recipientsArray = new String[to.size()];
                        String[] messageLinesArray = new String[messageLines.size()];
                        messageLines.toArray(messageLinesArray);
                        to.toArray(recipientsArray);
                        return new MailMessage(messageLinesArray,recipientsArray, topic,null);
                    }
                    break;
                case 5:
                    out.println("Wirklich Abbrechen?");
                    if(Input.GetYesNo()){
                        return null;
                    }


            }
        }
    }

    /**
     * Funktion zur Abänderung der Empfänger
     * @param list Eine Liste der Empfänger
     * @return die Abgeänderte Liste
     */
    @NotNull
    private  static List<String> EditRecipientList(@NotNull List<String> list){
        while(true){
            out.println("\n\n\n\n\n");
            out.println("[0] > Eingabe beenden");
            out.println("---");
            for(int i = 0;i<list.size();i++){
                out.println(String.format("[%d] > %s",i+1,list.get(i)));
            }
            out.println(String.format("[%d] > Neue Zeile hinzufügen",list.size()+1));
            Integer input = Input.GetIntegerFromRange(0,list.size()+1,false);
            if(input == null){  // Wird nicht null, benötigt um IntelliJ glücklich zu machen,
                input = -1;    // da Input.GetIntegerFromRange mit allowEmpty = true null zurückgeben kann
            }
            if(input == 0){
                if(list.size() == 0){
                    out.println("[FEHLER]: Es muss mindestens ein Empfänger angegeben werden");
                }else{
                    out.println("Eingabe wirklich beenden?");
                    if(Input.GetYesNo())
                        return list;

                }
            }else if(input == list.size()+1){
                out.println("Geben Sie den Inhalt der Zeile an... mit Enter wird die Zeile hinzugefügt");
                String inp = askForRecipientInput();
                list.add(inp);


            }
            else {
                out.println(String.format("Geben Sie den neuen Inhalt von Zeile %d an. Mit Enter wird die Zeile hinzugefügt",input));
                String inp = askForRecipientInput();
                if(inp == null)
                    inp = "";
                list.remove(input-1);
                list.add(input-1,inp);

            }
        }
    }

    /**
     * Eine Funktion zur Abänderung einer Liste an Strings. Diese wird zum ändern der Nachricht verwendet.
     * @param list Die Liste an Zeilen
     * @return Die Abgeänderte Liste
     */
    @NotNull
    private static List<String> EditStringList(@NotNull List<String> list){
        while(true){
            out.println("\n\n\n\n\n");
            out.println("[0] > Eingabe beenden");
            out.println("---");
            for(int i = 0;i<list.size();i++){
                out.println(String.format("[%d] > %s",i+1,list.get(i)));
            }
            out.println(String.format("[%d] > Neue Zeile hinzufügen",list.size()+1));
            Integer input = Input.GetIntegerFromRange(0,list.size()+1,false);
            if(input == null){  // Wird nicht null, benötigt um IntelliJ glücklich zu machen,
                input = -1;    // da Input.GetIntegerFromRange mit allowEmpty = true null zurückgeben kann
            }
            if(input == 0){
                if(list.size() == 0){
                    if(true){
                        return list;
                    }else{
                        out.println("[FEHLER]: Es muss mindestens eine Zeile eingegeben werden");

                    }
                }else{
                    out.println("Eingabe wirklich beenden?");
                    if(Input.GetYesNo())
                        return list;

                }
            }else if(input == list.size()+1){
                out.println("Geben Sie den Inhalt der Zeile an... mit Enter wird die Zeile hinzugefügt");
                String inp = Input.GetString(true);
                if(inp == null)
                    inp = "";
                list.add(inp);


            }
            else {
                out.println(String.format("Geben Sie den neuen Inhalt von Zeile %d an. Mit Enter wird die Zeile hinzugefügt",input));
                String inp = Input.GetString(true);
                if(inp == null)
                    inp = "";
                list.remove(input-1);
                list.add(input-1,inp);

            }
        }
    }

    /**
     * Überprüft anhand einer RegExp, ob es sich bei der Eingabe (in puncto Syntax) um eine valide Eingabe handelt.
     * Die dafür verwendete Regular Expression wurde bei emailregex.com gefunden.
     * @param s Der zu überprüfende String
     * @return Ob dieser String eine Email ist
     */
    public static boolean isMailString(String s){
        // Die Regex wurde von https://emailregex.com/ übernommen
        @RegExp
        String regex = "(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)])";
        Pattern regexPattern = Pattern.compile(regex);
        Matcher matcher = regexPattern.matcher(s);
        return matcher.find();
    }

    /**
     * Der Nutzer wird gefragt, einen Empfänger, also eine valide Email-Adresse, bzw \ einzugeben
     * @return die Adresse bzw null bei einer leeren Eingabe
     */
    @Nullable
    private static String askForRecipientInput(){
        String input = Input.GetString(false);
        if(input.equals("\\")){
            return null;
        }
        if(isMailString(input))
            return input;
        out.println("[FEHLER] Bei der Eingabe handelt es sich um keine Email oder \\");
        return askForRecipientInput();

    }
}

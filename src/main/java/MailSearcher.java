import org.jetbrains.annotations.NotNull;

import javax.mail.Address;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMultipart;
import javax.mail.search.SearchTerm;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


/**
 * Klasse zum Suchen von Nachrichten
 */
public class MailSearcher {
    /**
     * Privater Konstruktor, damit keine Instanz erstellt werden kann
     */
    private MailSearcher(){}


    /**
     * Suche in einem bestimmten Ordner nach Nachrichten.
     * @param folder Der durchzusuchende Ordner
     * @param searchString Der String, welcher gesucht werden soll
     * @return Nachrichten, in welchen der String beinhaltet ist
     */
    //https://www.codejava.net/java-ee/javamail/using-javamail-for-searching-e-mail-messages
    @NotNull
    public static Message[] SearchInFolder(@NotNull Folder folder, @NotNull String searchString){
        try{
            //Überprüfe zuerst, ob der Ordner Nachrichten beinhalten kann. Es gibt Ordner wie
            // [Gmail], welche as Wurzel dienen, aber selbst keine Ordner beinhalten
            if((folder.getType() & Folder.HOLDS_MESSAGES) == 0)
                return new Message[0]; // Ordner kann keine Nachricht beinhalten

            folder.open(Folder.READ_ONLY);
        }
        catch (MessagingException msgex){
            //Der "Ordner" [Gmail] kann keine Nachrichten beinhalten und dient eher als Wurzel für Unterordner
            if(!folder.getFullName().equals("[Gmail]"))
            System.out.println("[Fehler] Konnte Ordner "+folder.getFullName()+" nicht öffnen.");
            return new Message[0];
        }
        SearchTerm searchTerm = new SearchTerm() {
            //Der Override für match ist eine Methode, welche bei einem Match für eine Nachricht true zurückgibt, ansonsten false
            //Da wir überprüfen wollen, ob der Nachrichtentext, Betreff oder Absender oder  Empfänger (ausgehend) ein bestimmtes
            // Wort beinhalten, wird die Methode in dem Override dementsprechend Überschrieben
            @Override
            public boolean match(Message msg) {
                try {
                    if(msg.getSubject().contains(searchString))
                        return true;
                } catch (MessagingException e) {
                    return false; //Überspringe Nachricht
                }
                //https://stackoverflow.com/questions/11240368/how-to-read-text-inside-body-of-mail-using-javax-mail

                try {
                    //Überprüfe nur für text/plain Nachrichten
                    if(msg.isMimeType("text/plain") || msg.isMimeType("text/html")){
                        String msgBody = msg.getContent().toString();
                        if(msgBody.contains(searchString))
                            return true;
                    }
                    //Wenn es sich um eine Multipart-Nachricht handelt, rufe die Funktion auf, welche alle Text-Komponenten zusammen als einen String ausgibt
                    if(msg.isMimeType("multipart/*")){
                        String msgContent = JavaMailMessageUtils.GetMultipartTextContent((MimeMultipart)msg.getContent());
                        if(msgContent.contains(searchString))
                            return true;
                    }
                } catch (MessagingException | IOException e) {
                    return false;
                }
                //Überprüfe Absenden
                try{
                    Address[] from = msg.getFrom();
                    for(Address f : from){
                        if(f.toString().contains(searchString))
                            return true;
                    }
                }
                catch (MessagingException msgex){
                    return false;
                }
                //Überprüfe Empfänger
                try{
                    Address[] toCC = msg.getRecipients(Message.RecipientType.CC);
                    Address[] toTO = msg.getRecipients(Message.RecipientType.TO);
                    Address[] toBCC = msg.getRecipients(Message.RecipientType.BCC);
                    if(toCC != null)
                        for(Address to : toCC)
                            if(to.toString().contains(searchString))
                                return true;
                    if(toTO != null)
                        for(Address to : toTO)
                            if(to.toString().contains(searchString))
                                return true;
                    if(toBCC != null)
                        for(Address to: toBCC)
                        if(to.toString().contains(searchString))
                            return true;
                }
                catch (MessagingException msgex){
                    return false;
                }
                return false;
            }
        };
        try{
            Message[] matchedMessages = folder.search(searchTerm);
            try{
                if(folder.isOpen())
                    folder.close(false);
            }
            catch (MessagingException msgex){
                //
            }
            return matchedMessages;
        }
        catch (MessagingException msgex){
            System.out.println("[FEHLER] Konnte die Nachrichten nicht filtern");
            try{
                if(folder.isOpen())
                    folder.close(false);
            }
            catch (Exception e){
                //
            }
            return new Message[0];
        }

    }

    /**
     * Führt die Suche für alle angegebenen Ordner aus. Extrahiere dafür alle Nachrichten aus Ordners, welche
     * Nachrichten beinhalten können
     * @param folders Die Ordner, welche durchsucht werden sollen
     * @param searchString Der Suchstring
     * @return Die Nachrichten, welche den Suchstring beinhalten
     */
    @NotNull
    public static Message[] SearchInFolders(@NotNull Folder[] folders,@NotNull String searchString){
        List<Message> messageList = new ArrayList<>();
        for(Folder f : folders){
            try {
                //Wenn der Ordner keine Nachrichten haben kann: Überspringen zum nächsten Ordner
                if((f.getType() & Folder.HOLDS_MESSAGES) == 0)
                    continue;
                //messageList.add(SearchInFolder(f,searchString)) Leider kein Spread Operator in Java. :|
                for(Message m : SearchInFolder(f,searchString))
                    messageList.add(m);
            } catch (MessagingException e) {
                e.printStackTrace();
            }
        }
        Message[] messageArray = new Message[messageList.size()];
        messageList.toArray(messageArray);
        //Ich könnte mich darüber aufregen, wie hässlich und unnötig redundant dieses Konvertieren von List in Array ist,
        //aber dass Java veraltet ist sollte inzwischen allen klar sein.
        //Bspw. C# : list.ToArray();
        return messageArray;
    }
}

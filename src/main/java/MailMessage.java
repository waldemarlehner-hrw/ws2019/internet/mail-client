import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.StringJoiner;

/**
 * Instanz dieser Klasse repräsentiert eine Email-Nachricht. MailMessageFactory soll zum erstellen einer Instanz
 * verwendet werden. Beim Absenden wird die Nachricht in Javamail's Message Klasse konvertiert.
 */
public class MailMessage {
    public String[] To;
    public String From;
    public String Subject;
    public String[] MessageLines;

    MailMessage(@NotNull String[] msgLines, @NotNull String[] to, @NotNull String subject,@Nullable String from){
        To = to;
        From = from;
        MessageLines = msgLines;
        Subject = subject;
    }

    /**
     * Gebe die Nachricht mit allen Komponenten aus. Wenn die Nachricht von der MailMessageFactory erstellt wurde, hat diese für "From" den Wert null.in diesem Fall wird "Du" als Absender angegeben.
     */
    public void PrintMailMessage(){
        String sb = "----------------\n" +
                "Betreff > " +
                Subject +
                "\nVon > " +
                ((From == null)? "Du" : From )+
                "\nAn > \n" +
                GetRecipients() +
                "\n\nNachricht >\n" +
                GetMessage() +
                "\n----------------\n";
        System.out.println(sb);
    }

    /**
     * Die Nachricht wird in Zeilen gespaltet. Um die tatsächliche Nachricht zu bekommen wird die Nachricht wieder
     * zusammengesetzt.
     * @return Den Zusammengesetzten Textkörper
     */
    @NotNull
    public String GetMessage(){
        StringJoiner sj = new StringJoiner("\n");
        for(String s : MessageLines){
            sj.add(s);
        }
        return sj.toString();
    }

    /**
     * @return Gibt die Empfänger untereinender als String an.
     */
    @NotNull
    public String GetRecipients(){
        StringJoiner sj = new StringJoiner("\n");
        for (String s : To){
            sj.add(s);
        }
        return sj.toString();
    }


}

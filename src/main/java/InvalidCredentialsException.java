/**
 * Eine Exception, welche vom Connector geworfen wird, wenn die übergebenen Login-Daten keine Instanz von Connector erstelle können
 */
public class InvalidCredentialsException extends Exception {
    InvalidCredentialsException(String errMsg){
        super(errMsg);
    }
}

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;
import java.util.StringJoiner;
//Verwende Singleton Pattern
//Connector-Klasse. Soll als Schnittstelle zum Mailserver dienen

public class Connector {
    private static  Connector instance = new Connector();
    private Properties properties;
    private Session session;
    private Store store;

    //Privater Connector, damit man Klasse nicht initialisieren kann. Auf die Klasse soll über das Singleton-Pattern zugegriffen werden
    private Connector(){}
    public static Connector GetInstance(){
        return instance;
    }
    public Properties GetProperties(){return properties;}
    public  Session GetSession(){return session;}
    public String getUsername(){return username;}
    private String username,password;
    public void Init(String _username, String _password) throws InvalidCredentialsException{
        properties = System.getProperties();
        //https://support.google.com/mail/answer/7126229?hl=de Ports
        //SMTP zum Absenden von Nachrichten
        properties.put("mail.smtp.host","smtp.gmail.com");
        properties.put("mail.transport.protocol","smtp");
        properties.put("mail.smtp.port","587");
        properties.put("mail.smtp.starttls.enable","true"); //TLS wird benötigt, um zu überprüfen, ob die Login-Daten korrekt sind, bevor eine Nachricht gesendet wird. Zudem muss TLS oder SSH für eine Verbindung zu gmail genutzt werden
        properties.put("mail.smtp.auth","true");

        //https://stackoverflow.com/questions/34615522/failed-to-connect-to-gmail-using-imap
        //IMAP zum Abrufen von Nachrichten
        properties.put("mail.imaps.host","imap.gmail.com");
        properties.put("mail.imaps.port","993");
        properties.put("mail.imaps.auth","true");
        properties.put("mail.imaps.starttls.enable","true");
        //Die Verbindung zu GMail muss über TLS/SSL geschehen
        properties.put("mail.store.protocol", "imaps");


        password = _password;
        username = _username;

        //Code für javax.mail.Authenticator von der Hilfestellung "Email senden"

        session  = Session.getInstance(properties,new javax.mail.Authenticator(){
            protected PasswordAuthentication getPasswordAuthentication(){
                return new PasswordAuthentication(username,password);
            }
        });
        //Überprüfe über SMTP, ob die Login-Daten korrekt sind
        //Wenn dies nicht der Fall ist wird keine Instanz von Connector erzeugt
        if(!Authenticator.AreCredentialsValid(username,password,session)) {
            throw new InvalidCredentialsException("Failed to set up SMTP Connection with given credentials");
        }


        properties.setProperty("mail.user",username);
        properties.setProperty("mail.password",password);






    }

    /**
     * @param message Eine MailMessage Nachricht, welche Abgesendet werden soll.
     * @return Gibt zurück, ob das Senden der Nachricht erfolgreich war
     */
    //Verwendete Code-Teile : https://www.tutorialspoint.com/java/java_sending_email.htm
    public boolean SendMain(@NotNull MailMessage message){
        Message msg = new MimeMessage(session);
        try{
            if(MailMessageFactory.isMailString(username)){
                msg.setFrom(new InternetAddress(username));
            }else{
                msg.setFrom(new InternetAddress(username+"@gmail.com"));
            }
            msg.addRecipient(Message.RecipientType.TO,new InternetAddress(message.To[0]));
            for(int i = 1; i < message.To.length;i++){
                msg.addRecipient(Message.RecipientType.CC, new InternetAddress(message.To[i]));
            }
            msg.setSubject(message.Subject);
            {
                StringJoiner sj = new StringJoiner("\n");
                for(String s : message.MessageLines){
                    sj.add(s);
                }
                msg.setText(sj.toString());
                msg.setHeader("X-Mailer","smtpsend");
            }

        }catch (MessagingException msgex){
            System.out.println("[FEHLER] Es gab einen Fehler beim Erstellen der Nachricht");
            System.out.println(String.format("Grund: %s",msgex.getCause()));
        }
        try{
            //Sende die Nachricht über SMTP
            Transport transport = session.getTransport();
            transport.connect("smtp.gmail.com",username,password);
            transport.send(msg);
            transport.close();
            System.out.println("Nachricht gesendet.");
            return true;
        }catch (MessagingException msgex){

            System.out.println("[FEHLER] Es gab einen Fehler beim Senden der Nachricht");
            System.out.println(String.format("Grund: %s",msgex.getCause()));

        }
        return false;

    }

    /**
     * Suche alle Nachrichten im Absender, Empfänger, CC und BCC, sowie Textkörüer und Betreff nach einem Gegebenen String. Alle matches werden zurückgegeben
     * @param searchString Alle Nachrichten werden überprüft, ob diese den String beinhalten
     * @return Alle Nachrichten, welche den gegebenen String Matchen
     */
    //https://www.codejava.net/java-ee/javamail/using-javamail-for-searching-e-mail-messages
    @Nullable
    public Message[] SearchMailMessages(@NotNull String searchString){
        try{
            //Verbindung zu GMail
            Store store = session.getStore("imaps");
            store.connect(username,password);
            return MailSearcher.SearchInFolders(store.getDefaultFolder().list("*"),searchString);
        }
        catch (MessagingException msgex){
            System.out.println("[FEHLER] Konnte keine Verbindung über das IMAP Protokoll errichten. Somit konnten keine Nachrichten abgerufen werden");
            return null;
        }

    }
    /**
     * Funktion, bei welcher der Nutzer Ordner auswählen kann und in diesem Ordner operationen durchführen kann
     */
    public void FolderTraverse(){
        Folder root;
        try{
            Store store = session.getStore("imaps");
            store.connect(username,password);
            root = store.getDefaultFolder();

        }catch (Exception e){
            System.out.println("[FEHLER] Konnte keine Verbindung aufbauen");
            return;
        }
        JavaMailFolderUtils.DisplayFolderUserInput(root);
    }




}

# Mail-Client Internet WS 19/20

## Verwendung

Das Progframm ermöglicht die Abfrage von Nachrichten und Ordnern, die Suche aller Nachrichten
sowie das Schreiben neuer Nachrichten.
Hierfür werden Login-Daten für einen GMail-Account benötigt. Alternativ können folgenden Daten v
verwendet werden: 

Benutzername: `VMbq3X5eRsSbnDb3UWf4S6cF` Passwort: `gibreupotpyrrckp`

Das Programm startet mit einer Abfrage für die Login-Daten. Bei korrekter Eingabe wird das Menü angezeigt.
Dort kann man nun zwischen den Operationen `Ordner anzeigen`,`Nachricht schreiben` und `Suche` auswählen.

### Ordner anzeigen
Es werden alle Ordner mit einer ID mit der Anzahl der Nachrichten angezeigt. Nun kann man die ID angeben. 
Dadurch werden alle Nachrichten aus dem ausgewählten Ordner angezeigt.

Mit der Eingabe 0 gelangt man wieder zurück zum Hauptmenü. 

### Nachricht schreiben
Zuerst wird nach einem Betreff gefragt.

Daraufhin werden die Emfänger eingegeben. Die erste Eingabe wird der tatsächliche Empfänger,
die restlichen Eingaben werden als CC angefügt. 
Die Eingabe der Empfäger wird mit einem ` \ ` beendet.

Danach wird die Nachricht eingegeben. Neue Zeilen und sogar Leerzeilen sind möglich. Die Eingabe
der Nachricht wird mit einem ` \ ` abgeschlossen.

Daraufhin kann man bei Fehlern in der Eingabe den Betreff, die Empfänger als auch die 
Nachricht selbst noch ein mal korrigieren.  

### Suchen
Es wird ein String abgefragt nach welchem gesucht werden soll. Dann werden in allen Nachrichten 
der Betreff, der Textkörper sowie alle Empfänger nach dem eingegebenen String gesucht. 

Alle Nachrichten, welche von dieser Suchfunktion gematcht werden werden im Anschluss angezeigt.

## Klassen

Das Programm beinhaltet folgende Klassen

- Authenticator
    * Diese Klasse wird verwendet, um sich bei GMail zu authentifizieren. Sie sollte auch verwendet werden, um eine 
      Verbindung zu erstellen.
- Connector
    * Eine Klasse, welche das Singleton-Pattern implementiert und die Verbindung zum Mailserver repräsentiert.
      Über diese Klasse werden alle Aktionen, welche auf den Mailserver zugreifen, initiiert.
- Input
    * Eine statische Hilfsklasse, 
      welche Methoden für die Nutzereingabe bereitstellt.
- InvalidCredentialsException
    * Eine eigene Exception, welche von Connector geworfen wird wenn man versucht, den Connector mit falschen Logindaten
      zu Initialisieren.
- JavaMailFolderUtils
    * Eine statische Hilfsklasse, welche Hilfsfunktionen für das Arbeiten mit dem Folder-Objekt der JavaMail Library 
      bereitstellt.
- JavaMailMessageUtils
    * Eine statische Hilfsklasse, welche Hilfsunktionen für das Arbeiten mit dem Message-Objekt der JavaMail Library
      bereitstellt. Beinhaltet die Funktionen, um eine Nachricht auszugeben. 
- MailClient
    * Hierbei handelt es sich um den Startpunkt des Programms. Hier wird zu Beginn nach den Login-Daten gefragt. Bei 
      Eingabe korrekter Login-Daten wird das Hauptmenü angezeigt. 
      Im Hauptmenü lassen sich folgende Operationen auswählen
        * Ordner anzeigen und Nachrichten in aufgewähltem Ordner anzeigen
        * Textnachricht schreiben
        * Nach einem Text in allen Nachrichten suchen
- MailMessage
    * Eine Klasse zum speichern einer zu sendenden Nachricht. Zeilen der Nachrichtenkörpers sowie die Empfänger werden
      als Array abgespeichert. Dies macht das Bearbeiten der Nachricht einfacher.
- MailMessageFactory
    * Eine statische Hilfsklasse, welche Methoden zum Erstellen und Bearbeiten einer abzusendenen Nachricht 
    bereitstellt.
- MailSearcher
    * Eine statische Hilfsklasse, welche Methoden zur Suche von Nachrichten in einem Ordner oder mehreren Ordnern 
      anhand eines eingegebenen Suchstrings bereitstellt.
      
      
# Quellen

### Authenticator.java
[Überprüfen der Login-Daten über SMTP bevor eine E-Mail geschickt wird](https://stackoverflow.com/a/25499215)


### Connector.java 
[Suche bei Fehlerbehebung. GMail benötigt imaps anstatt imap, um eine Verbindung aufzubauen](https://stackoverflow.com/questions/34615522/failed-to-connect-to-gmail-using-imap)

[Ports und Einstellungen für IMAP und SMTP bei GMail](https://support.google.com/mail/answer/7126229?hl=de)

### JavaMailFolderUtils.java
[Erhalten aller Ordner welche Nachrichten beinhalten können](https://stackoverflow.com/questions/4790844/how-to-get-the-list-of-available-folders-in-a-mail-account-using-javamail)

[JavaMail Dokumentation zum Enum Folder.HOLDS_MESSAGES](https://javaee.github.io/javamail/docs/api/javax/mail/Folder.html#HOLDS_MESSAGES)

### JavaMailMessageUtils.java
[Rekursives Aufrufen des Körpers einer Multipart-Nachricht](https://stackoverflow.com/a/34689614)

### MailMessageFactory.java
[Regex zum überprüfen, ob ein String eine Email ist](https://emailregex.com/)

### MailSearcher.java
[Implementieren eines eigenen SearchTerm](https://www.codejava.net/java-ee/javamail/using-javamail-for-searching-e-mail-messages)

[Rekursives Aufrufen des Körpers einer Multipart-Nachricht](https://stackoverflow.com/a/34689614)
